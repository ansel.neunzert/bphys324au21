{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## BPHYS 324: Quantum Mechanics: The Infinite Square Well"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*This work is adapted with permission from a notebook by Dr. Joseph Paki. It was initially developed for PHYS 453 at the University of Michigan, Ann Arbor.*\n",
    "\n",
    "The Infinite Square Well (ISW) is one of the few problems in quantum mechanics that can be easily solved on a chalkboard. This makes it the usual starting place for introducing quantum concepts like energy levels, superposition, and stationary states.  In addition, having the \"exact\" answers makes the infinite square well problem a great introductory computational problem, since we can easily check that our program is working correctly!\n",
    "\n",
    "In this assignment we will:\n",
    "1. Plot the spatial parts of infinite square well eigenstates\n",
    "* Animate the time dependence of the ISW eigenstates\n",
    "* Form a superposition of ISW eigenstates and animate its time evolution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Infinite Square Well Review"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's review the equations we'll need. For an ISW located between $x=0$ and $x=a$, the solutions of the time-independent Schrodinger equation are\n",
    "$$\\psi_n(x)=\\sqrt{\\frac{2}{a}}\\sin \\left( \\frac{n\\pi}{a}x \\right)$$\n",
    "$$E_n = \\frac{n^2\\pi^2\\hbar^2}{2ma^2}$$\n",
    "$$\\Psi_n(x,t)=\\psi_n(x)e^{-iE_nt/\\hbar}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Therefore, an arbitrary state in an ISW can be written as a superposition of these eigenstates.\n",
    "$$\\Psi(x,t)=\\sum_{n=1}^{\\infty} c_n \\psi_n(x)e^{-iE_nt/\\hbar}$$\n",
    "$$c_n=\\int \\psi_n^*(x) \\Psi(x,t)dx$$\n",
    "$$\\sum_{n=0}^{\\infty}|c_n|^2=1$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time Scales and Small Numbers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Computers are great at math, but they often have trouble with very large or very small numbers.  We'd also like to plot things at a reasonable scale, sparing the reader from axes with numbers like $1.6\\times10^{-23}$.  Planck's constant ($\\hbar$), particle masses ($m$), and atomic distances ($a$) are very small numbers in MKS units, and we'd like to get a sense of the scales we're working with before programming away.  If you look at the above formulas, you'll see that the energy phase factor sets the time scale.  \n",
    "$$\\exp\\left( -i E_n t/ \\hbar\\right) = \\exp\\left( -i \\frac{n^2 \\pi^2 \\hbar}{2ma^2}t \\right)$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Question 1\n",
    "Suppose an electron is confined to an ISW with $a=1 \\unicode{x212B}= 1\\times 10^{-10}m$.  What is the period $T$ of the phase factor for the ground state?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that the typical time scales for an actual system are very small, so we'll choose units that let us work with reasonable numbers.  For the rest of the notebook, let's work in a new time scale, $$\\tau = \\frac{\\hbar}{ma^2}t$$ and furthermore say that $a=1\\mathrm{m}$.\n",
    "\n",
    "#### Question 2\n",
    "\n",
    "What is the magnitue of the multiplicative factor $\\frac{\\hbar}{ma^2}$ and what are its units?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Programming the Eigenstates"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's write some functions to get the eigenstates of the ISW as NumPy arrays.  This will make them easy to plot and manipulate.  First we need an array that represents our space, $x=0$ to $x=a=1$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy # This loads NumPy, or Numerical Python, from which we will use many tools.\n",
    "\n",
    "num_xpts = 100 # There are an infinite number of points between x=0 and x=1, but we're only going to model this many of them!\n",
    "x = numpy.linspace(0, 1, num_xpts)\n",
    "dx = x[1]-x[0] # This tells us the distance between points, and will be helpful later."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we will take our spatial locations $x$, and $n$, the quantum number, and returns the spatial part of the $n^{th}$ eigenstate, $\\psi_n(x_i)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def psi_n_x(x,n):\n",
    "    return numpy.sqrt(2)*numpy.sin(n*numpy.pi*x)    #Change this line to return the right function!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For those who haven't worked extensively with python before (or are less familiar with numpy), let's take a look at what we have now for $n=1$. Trying changing the parameter \"num_xpts\" and re-run to see how that affects the plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt \n",
    "plt.scatter(x,psi_n_x(x,1))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Okay, now let's check our function for $\\psi_n(x)$.  We'll do this in two ways, \n",
    "1. Checking that it is properly normalized \n",
    "* Plot it to make sure the boundary conditions and nodes are correct\n",
    "\n",
    "Here is a function that will take a function (in the form of a NumPy array), and return the integral of it's square, or the norm.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def norm(dx, f):\n",
    "    return dx*numpy.dot(f,numpy.conjugate(f))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Question 3\n",
    "We claim that the function \"norm\" above is equivalent to $ \\int_0 ^a f^*(x)f dx $. See if you can understand why.\n",
    "\n",
    "* Why are there no integration bounds in the function above?\n",
    "* Why is there a dot product instead of an integral?\n",
    "* $dx$ has an actual value here; it is not infinitesimally small. Why are we multiplying by $dx$?\n",
    "\n",
    "#### Question 4\n",
    "The following code will calculate the norm of the $n^{th}$ eigenstate and plot both the wavefunction and the probability distribution.  Change $n$ to check a few excited states (1, 2, 3, 4, etc...).\n",
    "\n",
    "Next, try turning $n$ to a really large number. When do the result start to look bad? Why might this be happening? *(Hint: No, physics is not broken, but our simulation is an approximation. What approximation is being made?)*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n=1    # Change this line to test different eigenstates\n",
    "psi_test = psi_n_x(x,n)\n",
    "print(\"Norm of psi_test = \",norm(dx, psi_test))\n",
    "\n",
    "plt.clf()\n",
    "plt.figure()\n",
    "plt.plot(x, psi_test)\n",
    "plt.xlabel(\"x\")\n",
    "plt.ylabel(\"psi_n(x)\")\n",
    "plt.title(\"Wavefunction of the Eigenstate, n=\"+str(n))\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(x, psi_test*numpy.conjugate(psi_test))    #Remember that the norm is conjugate(psi)*psi!\n",
    "plt.xlabel(\"x\")\n",
    "plt.ylabel(\"|psi_n(x)|^2\")\n",
    "plt.title(\"Probability Density of the Eigenstate, n=\"+str(n))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time evolution of each eigenstate\n",
    "Remember that so far we only have the *spatial* part of each eigenstate. We need to calculate its time evolution.\n",
    "\n",
    "Here's a function which, when given $x$, $n$, and $\\tau$ will return $\\psi_n(x,\\tau)$. Note that we save ourselves some work by reusing our function to calculate $\\psi_n(x)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def psi_n_x_t(x, n, t):\n",
    "    return psi_n_x(x,n)*numpy.exp(-1j*n**2 * numpy.pi**2 * t /2)\n",
    "# note: python uses \"j\" to represent sqrt(-1) rather than \"i\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Putting together the \"building blocks\" (time evolving eigenstates) into a time-evolving superposition state\n",
    "\n",
    "So far, we have only time-evolved the eigenstates. To make a superposition state, we will combine many eigenstates:\n",
    "\n",
    "$$\\Psi(x,\\tau)=\\sum_{n=1}^{\\infty} c_n \\psi_n(x)e^{-in^2\\pi^2\\tau/2} $$\n",
    "\n",
    "Now we are going to write a function that will take $x$, $\\tau$, and the eigenstate coefficients $c_n$ (as a NumPy array) as inputs, and return the superposition wavefunction at time $\\tau$, $\\Psi(x,\\tau)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def wavefunction(x, t, cn):\n",
    "    \n",
    "    # First we check that the coefficients obey the rule that total probability = 1\n",
    "    total_probability = numpy.dot(numpy.conjugate(cn),cn)\n",
    "    if(not abs(total_probability - 1) < 1e-6):    \n",
    "        print(\"Error! Sum of the conefficients squared does not equal 1! Total Probability =\", total_probability)\n",
    "        return\n",
    "    \n",
    "    # Now we create a blank wavefunction (a flat line at Psi(x)=0, basically).\n",
    "    wf = numpy.zeros_like(x)\n",
    "    for n in range(0,len(cn)): # for each n value (each term in the sum)...\n",
    "        wf = wf + cn[n]*psi_n_x_t(x, n+1, t) # we add the nth time-evolved eigenstate\n",
    "        \n",
    "    return wf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Animating $\\Psi(x,\\tau)$\n",
    "Now we can animate the time evolution of an arbitrary superposition! We just need to specify the coefficients $c_n$.  Here, we'll start off with an equal superposition of energy eigenstates $n=1,2,4,6$.  But you can create any mixture you like, as long as they obey $\\sum_n |c_n|^2=1$! You can include higher energy eigenstates by adding more elements to the $c_n$ array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cn=numpy.array([1/numpy.sqrt(2), 1/numpy.sqrt(2),0,0,0, 0,0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below is the code to animate $\\Psi(x,\\tau)$.  As is often the case, the code to do this takes up a lot of space, but it's just composed of a bunch of simple commands. (Ex. Setting axis labels, specifying the ranges, etc.) When you execute the cell, the code will use your \"wavefunction(x,t,cn)\" function to make the animation.  Three animations will appear, for $Re[\\Psi(x,\\tau)]$, $Im[\\Psi(x,\\tau)]$, and the probability density, $|\\Psi(x,\\tau)|^2$. \n",
    "\n",
    "Note: You press the power button on the top right of the animation to make it stop!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.animation as animation\n",
    "\n",
    "plt.clf()\n",
    "    \n",
    "# create a figure with two subplots\n",
    "fig, (ax1, ax2, ax3) = plt.subplots(3,1)\n",
    "\n",
    "# intialize two line objects (one in each axes)\n",
    "line1, = ax1.plot([], [], lw=2)\n",
    "line2, = ax2.plot([], [], lw=2, color='g')\n",
    "line3, = ax3.plot([], [], lw=2, color='r')\n",
    "line = [line1, line2, line3]\n",
    "\n",
    "# the same axes initalizations as before (just now we do it for both of them)\n",
    "ax1.set_ylim(-1.75, 1.75)\n",
    "ax1.set_xlim(0, 1)\n",
    "ax1.grid()\n",
    "ax1.set_ylabel(\"Re(Wavefunction)\")\n",
    "ax1.set_title(\"Evolution of Wavefunction and Probability Density in real space\")\n",
    "\n",
    "ax2.set_ylim(-1.75, 1.75)\n",
    "ax2.set_xlim(0, 1)\n",
    "ax2.grid()\n",
    "ax2.set_ylabel(\"Im(Wavefunction)\")\n",
    "\n",
    "ax3.set_ylim(0, 5)\n",
    "ax3.set_xlim(0, 1)\n",
    "ax3.grid()\n",
    "ax3.set_xlabel(\"x\")\n",
    "ax3.set_ylabel(\"Probability Density\")\n",
    "\n",
    "time_text = ax1.text(0.05, 0.95,'',horizontalalignment='left',verticalalignment='top', transform=ax1.transAxes)\n",
    "norm_text = ax1.text(0.05, 0.85,'',horizontalalignment='left',verticalalignment='top', transform=ax1.transAxes)\n",
    "time_step_per_frame = 0.001     # You could change this to make the animation faster or slower\n",
    "\n",
    "def run_init():\n",
    "    line[0].set_data([], [])\n",
    "    line[1].set_data([], [])\n",
    "    line[2].set_data([], [])\n",
    "    return line\n",
    "    \n",
    "def run(i):\n",
    "    wf = wavefunction(x, time_step_per_frame*i, cn)\n",
    "    y1data = numpy.real(wf)\n",
    "    y2data = numpy.imag(wf)\n",
    "    y3data = numpy.conjugate(wf)*wf\n",
    "\n",
    "    # update the data of the three line objects\n",
    "    line[0].set_data(x, y1data)\n",
    "    line[1].set_data(x, y2data)\n",
    "    line[2].set_data(x, y3data)\n",
    "    #Display the current animation time\n",
    "    time_text.set_text('tau = %.2f' % (i*time_per_frame))\n",
    "    norm_text.set_text('norm = %.2f' % (norm(dx,wf)))\n",
    "    return line, time_text, norm_text\n",
    "\n",
    "ani = animation.FuncAnimation(fig, run, init_func=run_init,\n",
    "                               frames=1000, interval=20, blit=True)\n",
    "    \n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Question 5\n",
    "Modify the coefficient array, $c_n$, up above so that you are animating an energy eigenstate. Describe what the animations of $Re[\\Psi(x,\\tau)]$, $Im[\\Psi(x,\\tau)]$, and the probability density, $|\\Psi(x,\\tau)|^2$ look like.  After that, feel free to play around with the coefficients to get all sorts of funky superposition behavior.\n",
    "\n",
    "#### Question 6\n",
    "Come up with a coefficient array that (a) is properly normalized and (b) has at least 2 different nonzero entries. Write down the array and describe its time evolution.\n",
    "\n",
    "### Representations and bases\n",
    "Notice that each time you run this notebook, you are representing the wavefunction in two different ways. The first way is through a series of $c_n$ values, represented as a vector. (NumPy prints this in a row, whereas in class we would write it in a column-- you can ignore this difference.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(cn)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Question 7\n",
    "In what *basis* is the array above?\n",
    "\n",
    "#### Question 8\n",
    "The array above should \"really\" be infinitely long - why? Would we make our simulation better by extending the array to, say, 1 billion entries? Why or why not? What information would the new entries contain?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The second representation of the wavefunction is a *position* representation. Let's print it out at t=0:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wf = wavefunction(x, 0, cn)\n",
    "print(wf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Question 9\n",
    "What do you notice about the real and imaginary parts of each entry in the array above? Why is it like this?\n",
    "\n",
    "#### Question 10\n",
    "What does each value in the array above represent?\n",
    "\n",
    "#### Question 11\n",
    "The array above should \"really\" be infinitely long - why? Would we make our simulation better by extending the array to, say, 1 billion entries? Why or why not? What information would the new entries contain?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
