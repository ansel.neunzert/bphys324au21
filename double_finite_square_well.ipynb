{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# BPHYS 324:  Double Finite Square Well"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*This work is adapted with permission from a notebook by Dr. Joseph Paki. It was initially developed for PHYS 453 at the University of Michigan, Ann Arbor.*\n",
    "\n",
    "The Time Independent Schrodinger equation is\n",
    "$$ E\\Psi(x)=\\left( \\frac{-\\hbar^2}{2m} \\nabla^2 + V(x)\\right) \\Psi(x) $$\n",
    "We will solve this equation numerically to obtain the energy eigenstates of the double finite square well, as in Problem 2.47 in Griffiths.  In one dimension, we can rearrange the equation to read\n",
    "$$\\psi''(x) =  -\\frac{2m}{\\hbar^2}\\left[ E - V(x)\\right]\\psi(x)$$\n",
    "Where for the double finite square well\n",
    "$$V(x)=-V_0,\\; \\frac{b}{2}<|x|<\\frac{b}{2}+a$$\n",
    "$$V(x)=0,\\; otherwise$$\n",
    "We'll set $m=\\hbar=1$, so that we must solve\n",
    "$$\\psi''(x) =  2\\left[ V(x)-E\\right]\\psi(x)$$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Question 1\n",
    "\n",
    "**Draw a diagram showing the double finite square well. Label the origin and the distances $a$ and $b$. The vertical axis should show potential, with $V(x)=0$ and $V(x)=-V_0$ clearly labeled.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Finite Differences\n",
    "On a computer, we must work with discretized spatial grids.  Since the potential is symmetric, the eigenstates will be either symmetric or anti-symmetric, so we only have to solve for \"half\" of the wavefunction, from $x=0$ to some large $x=L$.  We will solve for the wavefunction on a discrete grid with $N$ points, defined by\n",
    "$$ x_i = i\\times\\delta x$$\n",
    "$$\\delta x = \\frac{L}{N}$$\n",
    "Furthermore, we need to use finite differencing to calculate derivatives numerically on a discrete grid.  For example, the forward finite difference scheme for the first derivative is\n",
    "$$\\psi'(x_i) \\approx \\frac{\\psi(x_{i+1})- \\psi(x_i)}{\\delta x}$$\n",
    "While the backward differencing scheme is\n",
    "$$\\psi'(x_i) \\approx \\frac{\\psi(x_{i})- \\psi(x_{i-1})}{\\delta x}$$\n",
    "By using both the forward and backward differencing schemes, we can obtain what's called the second-order central difference approximation to the second derivative \n",
    "\n",
    "$$\\psi''_i \\approx (\\psi'_i)'=\\left( \\frac{\\psi_{i+1} - \\psi_i}{\\delta x} \\right)'=\\frac{(\\psi_{i+1}-\\psi_i) - (\\psi_i - \\psi_{i-1})}{\\delta x^2}$$\n",
    "\n",
    "$$\\psi''_i \\approx \\frac{\\psi_{i+1} - 2\\psi_i + \\psi_{i-1}}{\\delta x ^2}$$\n",
    "where $\\psi_i=\\psi(x_i)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Shooting Method\n",
    "Let's plug our our expression for the discrete second order derivative into the Schrodinger equation. After rearranging a bit, we get\n",
    "$$ \\psi_{i+1} = 2\\psi_i - \\psi_{i-1} + 2\\delta x^2 \\left[ V(x_i) -E \\right] \\psi_i$$\n",
    "The solutions are either symmetric or anti-symmetric, so either $\\psi_0$ or $\\psi_0'$ will be zero. Since \n",
    "$$\\psi_1 \\approx \\psi_0 + \\delta x \\psi_0'$$\n",
    "We can start with either $\\psi_0$ or $\\psi_0'$, and then use the previous two formulas to construct the rest of the wavefunction.  This is called the Shooting Method.\n",
    "\n",
    "Note: How do we know $\\psi_0$ or $\\psi_0'$? Turns out, it doesn't matter! We just guess a value, and the only effect the value has is on the normalization of the resulting wavefunction, which we can easily correct. \n",
    "\n",
    "From our physical intuition (gained from Problem 2.47 in Griffiths), the ground state should be symmetric, with $\\psi_0'=0$, while the first excited state will be anti-symmetric, with $\\psi_0=0$. \n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Obtaining the Energy\n",
    "Wait, but what about the energy, $E$?  We don't know that either, but it appears as a parameter in the Shooting Method. Furthermore, although we can solve the Schodinger equation for any $E$, only very particular energies will yield physical, normalizable states.\n",
    "\n",
    "Here's how we get around this: we start with an energy range, $E\\in \\left[E_{min}, E_{max} \\right]$, which we think contains the physical states.  Then we search this range with a method like [bisection](https://en.wikipedia.org/wiki/Bisection_method) until we find the 'real' energy to an arbitrary degree of accuracy. Note:  The details of doing this are tricky! \n",
    "\n",
    "With this method, we'll never get exactly the right energy, which means the wavefunction will still be a bit off and blow up at large $x$.  But, we'll be so (arbitrarily) close to the right energy that the wavefunction near the double well will be extremely close to the right one. So, once we get to our desired accuracy, we just window the wavefunction, manually setting the values beyond a certain distance from the center of the wells to zero, and normalizing what remains."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Question 2\n",
    "\n",
    "**Explain the basic idea of the shooting method in your own words. You should address each of the following:**\n",
    "\n",
    "* **For each trial value of $E$, how is the wavefunction computed? Explain, using at least 2 sentences and a diagram.**\n",
    "\n",
    "* **For each trial value of $E$, how do we tell whether the wavefunction is a physically valid solution? Explain, using at least 2 sentences and a diagram.**\n",
    "\n",
    "* **If a trial value of $E$ does *not* yield a physically valid wavefunction, what is done next? Explain, using at least 2 sentences and a diagram.**\n",
    "\n",
    "* **How is this procedure similar to the analytical approach for solving a well? How is it different? Explain using at least 2 sentences.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt \n",
    "\n",
    "#%matplotlib notebook\n",
    "#import matplotlib\n",
    "#import matplotlib.pyplot as plt \n",
    "from matplotlib.widgets import Slider, Button, RadioButtons\n",
    "import numpy\n",
    "import math"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Small functions to get the double well potential and normalize wavefunctions.  \n",
    "1. *Potential* takes an array of the grid points and returns an array of the same length with the values of the potential at each point, $V(x_i)=V_i$.  \n",
    "*    *norm* takes an array of values for the wavefunction $\\psi(x_i)=\\psi_i$ and returns its norm, $\\int \\psi^2dx \\approx \\sum_i \\psi_i^2 \\delta x$. \n",
    "*    *normalize* will normalize the wavefunction so that $\\sum_i \\psi_i^2 \\delta x=1$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def Potential(x, a, b, V0):\n",
    "    V=numpy.zeros(len(x))\n",
    "    V[(abs(x)>=b/2.) & (abs(x)<b/2.+a)] = -V0\n",
    "    return V\n",
    "\n",
    "def norm(dx, psi):\n",
    "    return dx*numpy.dot(psi,psi)\n",
    "\n",
    "def normalize(dx, psi):\n",
    "    return psi/numpy.sqrt(norm(dx,psi))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is the meat of the program.  Starting with the value of the wavefunction and its derivative at $x=0$, we use the shooting method to calculate the rest of the wavefunction iteratively, changing the energy until we find an acceptable solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def forward_shooting(V, x, dx, f0, df0, Emin, Emax, acc):\n",
    "    \"\"\"\n",
    "    V - array with the values of the potential at the points in x\n",
    "    x - the grid points\n",
    "    dx - the spacing of the grid\n",
    "    f0 - value of the funtion at x=0\n",
    "    df0 - value of the derivative of the function at x=0\n",
    "    Emin, Emax - the energy window in which to search for physical solutions\n",
    "    acc - find the energy to within this accuracy, smaller accuracy will require more iterations\n",
    "    \"\"\"\n",
    "    Edelta = 1.\n",
    "    E = Emin\n",
    "    psi = numpy.zeros(len(x))\n",
    "    psi[0]=f0\n",
    "    max_steps = 500\n",
    "    count=0\n",
    "    \n",
    "    #Find the energy corresponding to a physical state via bisection\n",
    "    #Keep iterating until we have the energy to within acc, or until we take max_steps iterations\n",
    "    while Edelta > acc and count < max_steps:\n",
    "        count+=1\n",
    "        #Step through the grid points and find the value of the wavefunction from the shooting method\n",
    "        for i, x_ in enumerate(x[0:-1]):\n",
    "            if i==0:\n",
    "                psi[i+1] = psi[i] + dx*df0\n",
    "            else:\n",
    "                psi[i+1] = -psi[i-1] + 2.*( 1 + dx**2 * (V[i] - E) )*psi[i]\n",
    "                \n",
    "            #This is the tricky part! If the wavefunction is blowing up,\n",
    "            #we use bisection to change the energy and search for the physical energy.\n",
    "            #The details of how to pick the next energy depend on number of nodes the wavefunction has.  \n",
    "            #This will work for n=0,1,4,5,8,9...\n",
    "            if(psi[i] > 25):\n",
    "                Emin=E\n",
    "                E = Emin + (Emax-Emin)/2.\n",
    "                break\n",
    "            elif(psi[i] < -25):\n",
    "                Emax=E\n",
    "                E=Emax-(Emax-Emin)/2.\n",
    "                break\n",
    "        Edelta = (Emax-Emin)\n",
    "    return (psi, E)\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This function just nicely wraps everything up: when we call it with the double well parameters, we get the ground and first excited states back, plus their energies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "def acquire_double_well_states(a, b, V0, x, dx, acc, L_plot):\n",
    "    V = Potential(x, a, b, V0)\n",
    "    \n",
    "    #Get the ground state (symmetric, psi'(x=0)=0)\n",
    "    (psi_0, energy_0) = forward_shooting(V, x, dx, 1, 0, -V0, 0, acc)\n",
    "    \n",
    "    #Get the first excited state (antisymmetric, psi(x=0)=0)\n",
    "    (psi_1, energy_1) = forward_shooting(V, x, dx, 0, 1, -V0, 0, acc)\n",
    "    psi_0[x>L_plot]=0\n",
    "    psi_1[x>L_plot]=0\n",
    "\n",
    "    #Use symmetry to get the other half of the wavefunction\n",
    "    space = numpy.zeros(2*len(x))\n",
    "    space[0:len(x)]=-x[::-1]\n",
    "    space[len(x):2*len(x)]=x\n",
    "\n",
    "    Psi_0 = numpy.zeros(len(space))\n",
    "    Psi_0[0:len(x)]=psi_0[::-1]\n",
    "    Psi_0[len(x):2*len(x)]=psi_0\n",
    "    Psi_0=normalize(dx, Psi_0)\n",
    "\n",
    "    Psi_1 = numpy.zeros(len(space))\n",
    "    Psi_1[0:len(x)]=psi_1[::-1]\n",
    "    Psi_1[len(x):2*len(x)]=-psi_1\n",
    "    Psi_1=normalize(dx, Psi_1)\n",
    "    \n",
    "    return (space, Psi_0, energy_0, Psi_1, energy_1)\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's check the solutions for a particular $b$.  Notice that both states are properly normalized."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "a=1.5\n",
    "b=0\n",
    "V0=2\n",
    "dx=0.05\n",
    "acc=1e-10\n",
    "L=10*(b+a)\n",
    "L_plot=10\n",
    "x=numpy.linspace(0,L,int(L/dx+1),True)\n",
    "\n",
    "(space, Psi_0, energy_0, Psi_1, energy_1) = acquire_double_well_states(a, b, V0, x, dx, acc, L_plot)\n",
    "\n",
    "print(\"Norm Psi_0=\",norm(dx, Psi_0))\n",
    "print(\"Energy_0=\", energy_0)\n",
    "print(\"Norm Psi_1=\",norm(dx, Psi_1))\n",
    "print(\"Energy_1=\", energy_1)\n",
    "\n",
    "plt.subplots()\n",
    "plt.plot(space, Potential(space, a, b, V0), label=\"V(x)\")\n",
    "\n",
    "plt.plot(space, Psi_0, 'r',label=\"$\\psi_0$\")\n",
    "plt.plot([-L_plot,L_plot], [energy_0,energy_0],'r--',label=\"$E_0$\")\n",
    "plt.plot(space, Psi_1, 'g',label=\"$\\psi_1$\")\n",
    "plt.plot([-L_plot,L_plot], [energy_1,energy_1],'g--',label=\"$E_1$\")\n",
    "\n",
    "plt.xlim([-L_plot,L_plot])\n",
    "plt.ylim([-2,0.6])\n",
    "plt.legend(loc=3)\n",
    "plt.title('Ground and First Excited States for the Double Well')\n",
    "plt.xlabel('x')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's create an interactive plot, where we can change the well separation, $b$, and see how the states and energies change."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "plt.subplots_adjust(left=0.15, bottom=0.15)\n",
    "\n",
    "#Set up an initial solution to plot\n",
    "b0=0\n",
    "(space, Psi_0, energy_0, Psi_1, energy_1) = acquire_double_well_states(a, b0, V0, x, dx, acc, L_plot)\n",
    "\n",
    "pot, = plt.plot(space, Potential(space, a, b0, V0), label=\"$V(x)$\")\n",
    "state_0, = plt.plot(space, Psi_0, 'r',label=\"$\\psi_0$\")\n",
    "E0, = plt.plot([-L_plot,L_plot], [energy_0,energy_0],'r--',label=\"$E_0$\")\n",
    "state_1, = plt.plot(space, Psi_1, 'g',label=\"$\\psi_1$\")\n",
    "E1, = plt.plot([-L_plot,L_plot], [energy_1,energy_1],'g--',label=\"$E_1$\")\n",
    "\n",
    "plt.xlim([-L_plot,L_plot])\n",
    "plt.ylim([-2,0.7])\n",
    "plt.legend(loc=3)\n",
    "plt.title('Ground and First Excited States for the Double Well')\n",
    "plt.xlabel('x')\n",
    "\n",
    "#Add a slider to interactively change the well separation, b\n",
    "axb = plt.axes([0.25, 0.04, 0.65, 0.03])\n",
    "sb = Slider(axb, 'b', 0., 4., valinit = b0)\n",
    "\n",
    "#When we move the slider, calculate the solutions for the new b\n",
    "def update(val):\n",
    "    bnew = sb.val\n",
    "    (space, Psi_0, energy_0, Psi_1, energy_1) = acquire_double_well_states(a, bnew, V0, x, dx, acc, L_plot)\n",
    "    pot.set_ydata(Potential(space, a, bnew, V0))\n",
    "    state_0.set_ydata(Psi_0)\n",
    "    E0.set_ydata([energy_0,energy_0])\n",
    "    state_1.set_ydata(Psi_1)\n",
    "    E1.set_ydata([energy_1,energy_1])\n",
    "    \n",
    "sb.on_changed(update)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Question 3\n",
    "\n",
    "**What do you observe about the energies when $b$ is large? Explain why this makes sense.**\n",
    "\n",
    "**What do you observe about the energies when $b$ is small (near zero)? Explain why this makes sense.**\n",
    "\n",
    "# Question 4\n",
    "\n",
    "**Imagine that the centers of the wells are free to move around (that is, if $b$ could be freely adjusted). In the symmetric (red) state, is it energetically favorable for them to be close together, or far apart?**\n",
    "\n",
    "**What about in the symmetric (green) state?**\n",
    "\n",
    "# Question 5\n",
    "\n",
    "**Skip ahead in your textbook to figures 13.14 and 13.15 (p. 439-440). These figures show the behavior of a hydrogen *molecular ion* (one electron and two nuclei, net charge of +1).**\n",
    "\n",
    "**Figure 13.15 (a) and (c) show 1D cross sections of the wavefunction for the lowest-energy bonding (\"gerade\") and anti-bonding (\"ungerade\") molecular orbitals. Compare and contrast this plot with the double finite square well.**\n",
    "\n",
    "**Figure 13.14 shows the energies as a function of nuclear separation for the lowest-energy bonding (\"gerade\") and anti-bonding (\"ungerade\") molecular orbitals. Compare and contrast this plot with your answers to questions (3) and (4).**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### References\n",
    "1. [Shooting Method for Double Well](https://nbviewer.jupyter.org/urls/www.numfys.net/media/notebooks/eigenenergies_double_well_potential.ipynb), Henning G. Hugdal, Magnus H-S Dahle and Peter Berg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Appendix\n",
    "Here we check the energies for the single well (spacing of zero) and that the normalization is correct."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "a=1.5\n",
    "(space, Psi_0, energy_0, Psi_1, energy_1) = acquire_double_well_states(a, 0, V0, x, dx, acc, L_plot)\n",
    "\n",
    "print(\"Norm Psi_0=\",norm(dx, Psi_0))\n",
    "print(\"Energy_0=\", energy_0)\n",
    "print(\"Norm Psi_1=\",norm(dx, Psi_1))\n",
    "print(\"Energy_1=\", energy_1)\n",
    "\n",
    "plt.subplots()\n",
    "plt.plot(space, Potential(space, a, b, V0), label=\"V(x)\")\n",
    "\n",
    "plt.plot(space, Psi_0, 'r',label=\"$\\psi_0$\")\n",
    "plt.plot([-L_plot,L_plot], [energy_0,energy_0],'r--',label=\"$E_0$\")\n",
    "plt.plot(space, Psi_1, 'g',label=\"$\\psi_1$\")\n",
    "plt.plot([-L_plot,L_plot], [energy_1,energy_1],'g--',label=\"$E_1$\")\n",
    "\n",
    "plt.xlim([-L_plot,L_plot])\n",
    "plt.ylim([-2,0.6])\n",
    "plt.legend(loc=3)\n",
    "plt.title('Ground and First Excited States for the Double Well')\n",
    "plt.xlabel('x')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
